import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { HeaderComponent } from './header/header.component';
import { NavSidebarComponent } from './nav-sidebar/nav-sidebar.component';
import { ContentDateComponent } from './content-date/content-date.component';


@NgModule({
  declarations: [
    HeaderComponent,
    NavSidebarComponent,
    ContentDateComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    HeaderComponent,
    NavSidebarComponent,
    ContentDateComponent,
  ]
})
export class SharedModule { }
