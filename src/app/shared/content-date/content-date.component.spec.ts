import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentDateComponent } from './content-date.component';

describe('ContentDateComponent', () => {
  let component: ContentDateComponent;
  let fixture: ComponentFixture<ContentDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
