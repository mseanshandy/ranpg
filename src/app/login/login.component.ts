import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  rotateStatus = false;
  visibilityStatus = false;
  passwordVisibility = 'password';
  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
    captcha: new FormControl('')
  });

  constructor(private router: Router) { }

  ngOnInit() {
  }

  reloadCaptcha() {
    this.rotateStatus = !this.rotateStatus;
  }

  toggleVisibility() {
    this.visibilityStatus = !this.visibilityStatus;

    this.visibilityStatus ? this.passwordVisibility = 'text' : this.passwordVisibility = 'password';
  }

  doLogin() {
    this.router.navigate(['/home']);
  }
}
