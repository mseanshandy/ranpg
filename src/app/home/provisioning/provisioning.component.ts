import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-provisioning',
  templateUrl: './provisioning.component.html',
  styleUrls: ['./provisioning.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProvisioningComponent implements OnInit {
  searchForm = new FormGroup({
    search: new FormControl(''),
  });

  filterForm = new FormGroup({
    filter: new FormControl(''),
  });

  constructor(private router: Router) { }

  ngOnInit() {
  }

  search() {
    console.log(this.searchForm.get('search').value);
  }

  filter() {
    console.log('filter');
  }
}
