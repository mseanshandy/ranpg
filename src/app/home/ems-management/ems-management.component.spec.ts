import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmsManagementComponent } from './ems-management.component';

describe('EmsManagementComponent', () => {
  let component: EmsManagementComponent;
  let fixture: ComponentFixture<EmsManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmsManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
