import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfigDataComponent } from './config-data/config-data.component';
import { EmsManagementComponent } from './ems-management/ems-management.component';
import { LicenseComponent } from './license/license.component';
import { NetworkElementComponent } from './network-element/network-element.component';
import { ProvisioningComponent } from './provisioning/provisioning.component';
import { SchedulerComponent } from './scheduler/scheduler.component';
import { UserManagementComponent } from './user-management/user-management.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'configuration-data', component: ConfigDataComponent },
  { path: 'ems-management', component: EmsManagementComponent },
  { path: 'license', component: LicenseComponent },
  { path: 'network-element', component: NetworkElementComponent },
  { path: 'provisioning', component: ProvisioningComponent },
  { path: 'user-management', component: UserManagementComponent },
  { path: 'scheduler', component: SchedulerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
