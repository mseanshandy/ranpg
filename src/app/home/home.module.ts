import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';


import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfigDataComponent } from './config-data/config-data.component';
import { EmsManagementComponent } from './ems-management/ems-management.component';
import { LicenseComponent } from './license/license.component';
import { NetworkElementComponent } from './network-element/network-element.component';
import { ProvisioningComponent } from './provisioning/provisioning.component';
import { SchedulerComponent } from './scheduler/scheduler.component';
import { UserManagementComponent } from './user-management/user-management.component';


@NgModule({
  declarations: [
    DashboardComponent,
    ConfigDataComponent,
    EmsManagementComponent,
    LicenseComponent,
    NetworkElementComponent,
    ProvisioningComponent,
    SchedulerComponent,
    UserManagementComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ]
})
export class HomeModule { }
